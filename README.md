# Une solution

## Objectif

Pourvoir construire en une commande un container capable de servir le html et l'api en http ainsi qu'un lien vers l'apk.

## Mise en place

* migrer votre dépos vers [gitlab.com](gitlab.com) dans un projet publique
* récupérer les fichier [.gitlab-ci.yml](.gitlab-ci.yml), [Dockerfile](Dockerfile), [Deliverous](Deliverous)
* créez un compte sur [Deliverous.com](https://deliverous.com)
* sur [Deliverous.com](https://deliverous.com), créez un projet préfixé de `IUT2019-` ça évitera qu'il ne soit détruit au bout de 30 jours
* dans ce nouveau projet, cliquer sur **Nouveau Trigger** vous allez obtenir une URL de la forme : `https://deliverous.com:443/api/v1/trigger/1234abcd-abcd-abcd-xxxx-03ba2e9753e2` Récupérer la dernière partie, c'est le token d'authentification pour déployer votre projet.
* toujours dans ce nouveau projet, cliquez sur **Nouvelle IP**
* Retour sur le projet sur [gitlab.com](gitlab.com) allez dans **Settings - CI/CD** puis dans **Environment variables** ajouter une variable `DELIVEROUS_TRIGGER` avec comme valeur le token de déploiement précédement récupéré.
* Mettre à jour le fichier [Deliverous](Deliverous) avec l'URL de votre propre projet gitlab et l'IP que vous avez obtenu sur [Deliverous.com](https://deliverous.com)
* Dans le fichier [.gitlab-ci.yml](.gitlab-ci.yml) enlever le `.` devant `deploy` pour activer l'étape de déploiement.
