# Serveur Java

Le serveur Java fourni dans ce dossier correspond à la solution du dernier TP de programmation répartie. Il permet notamment de :
- fournir une api REST/JSON sur l'url `http://localhost:8080/api/v1/xxxxx` :
	- http://localhost:8080/api/v1/pizzas permet de manipuler la liste des pizzas
	- http://localhost:8080/api/v1/ingredients permet de manipuler la liste des ingrédients
- servir les fichiers statiques de l'application sur http://localhost:8080/ : lorsque l'utilisateur charge cette URL, le serveur retourne le fichier `index.html` contenu dans le dossier `server/src/main/resources/static`.

	***NB:*** le fichier `index.html` a besoin du fichier `main.bundle.js` pour fonctionner. Ce fichier est généré par la compilation du projet JS contenu dans le dossier `client-js` : voir [README](../client-js/README.md)

##  Développement sous eclipse

File 'import ' -> maven >> Existing maven projects
Parcourir jusque le répertoire 'server'
sélectionner le projet /pom.xml fr.ulille.iut.squelette...

## Développement sous IntelliJ

  ...TODO...
  
## Lancement du serveur
```bash
cd /chemin/vers/dossier/server/
mvn compile # compilation
mvn exec:java # execution (version main)
mvn package # création d'un jar exécutable
mvn test # évaluation des tests (version test)
```

Les ressources utilisées sont spécifiques (main ou test):
* persistence.xml (h2/fichier pour main, h2/memoire pour test)
**   La base de données cible
  ...h2:file:/tmp/appli (base dans le fichier /tmp/appli.h2.db)
  ...h2:mem:appli  (base de données en mémoire)
**   Le niveau de Log des accès SQL. Ces logs sont désactivés dans le projet initial
  dans les dernières lignes DES fichier persistence.xml, remplacer OFF par FINE ou FINEST selon le niveau de verbosité voulu

  
   
* log4j et logback (définition des logs)


## Structure du projet:

```
├── pom.xml
├── README.md
├── src
│   ├── main
│   │   ├── java
│   │   │   └── fr
│   │   │       └── ulille
│   │   │           └── iut
│   │   │               └── pizzaland              
│   │   │                   ├── ApiV1.java                         Application web
│   │   │                   ├── CORSFilter.java              
│   │   │                   ├── dao
│   │   │                   │   ├── DataAccess.java                Facade BDD
│   │   │                   │   ├── IngredientEntity.java       |
│   │   │                   │   ├──  .....                      |  Entités java
│   │   │                   │   └── PizzaEntity.java            |
│   │   │                   ├── dto
│   │   │                   │   ├── IngredientDto.java          |
│   │   │                   │   ├── IngredientPayloadDto.java   |  
│   │   │                   │   ├── PizzaCreateDto.java         |   .. Dto(s)
│   │   │                   │   ├── PizzaDto.java               |
│   │   │                   │   └── PizzaShortDto.java          |
│   │   │                   ├── Main.java                          Server complet
│   │   │                   └── ressources
│   │   │                       ├── IngredientRessource.java    |
│   │   │                       ├── MyResource.java             |  Ressources
│   │   │                       ├── PizzaRessource.java         |
│   │   │                       └── UncaughtException.java
│   │   └── resources
│   │       ├── log4j.properties     | 
│   │       ├── log4j.xml            | Définition des logs
│   │       ├── logback.xml          |
│   │       ├── META-INF
│   │       │   ├── persistence.xml  | Propriétés de la couche ORM (JPA) (h2, Postgres, ...)
│   │       │   └── sql
│   │       │       └── populate.sql | script d'intialisation de la base
│   │       └── static               | Fichiers statiques
│   │           ├── build
│   │           │   ├── main.bundle.js        | Résultat de compilation js
│   │           │   └── main.bundle.js.map    | 
│   │           ├── css
│   │           │   ├── flatly-bootstrap.css
│   │           │   ├── fonts
│   │           │   │   └── glyphicons-halflings-regular.woff2
│   │           │   └── main.css
│   │           ├── images
│   │           │   ├── bg.png
│   │           │   ├── carbonara.jpg
│   │           │   ├── hawaienne.jpg
│   │           │   ├── hot.svg
│   │           │   ├── logo.svg
│   │           │   ├── napolitaine.jpg
│   │           │   ├── oranaise.jpg
│   │           │   ├── regina.jpg
│   │           │   ├── savoyarde.jpg
│   │           │   └── spicy.jpg
│   │           └── index.html
│   └── test
│       ├── http         | illustration des accés à l'API (documentation)
│       │   ├── ingredients
│       │   │   ├── delete_id_404.http
│       │   │   ├── ....
│       │   │   └── put.http
│       │   ├── pizzas
│       │   │   ├── delete_id_404.http
│       │   │   ├── ....
│       │   │   └── put.http
│       │   └── test_serveur.http
│       ├── java
│       │   └── fr
│       │       └── ulille
│       │           └── iut
│       │               └── pizzaland              | méthodes de test
│       │                   ├── ressources
│       │                   │   ├── IngredientRessourceTest.java
│       │                   │   ├── ....
│       │                   │   └── StaticFilesTest.java
│       │                   └── testdao
│       │                       ├── AllTestsDisabled.java
│       │                       ├── ....
│       │                       └── PizzaJpaTestDisabled.java
│       └── resources ' Structure équivalente à la version 'main'
│           │       .......
│           └── static
```

## Trucs et astuces

tester/vérifier la BDD

Prérequis: archives h2 accessible (http://central.maven.org/maven2/com/h2database/h2/1.4.197/h2-1.4.197.jar)
On suppose que la variable shell h2 donne le cheminde ce fichier (export h2=...jar)

Par défaut la basse utilisée est localisé comme un fichier de nom ~/tmp/pizzaland.mv.db
Les éventuelles commandes SQL échouées sont stockées dans ~/tmp/pizzaland.mv.trace.db

Un shell sur la base de données paut être ouvert par la commande
```
> java -cp h2-*.jar org.h2.tools.Shell

Welcome to H2 Shell
Exit with Ctrl+C
[Enter]   jdbc:h2:mem:2
URL       jdbc:h2:./path/to/database
[Enter]   org.h2.Driver
Driver
[Enter]   sa
User      your_username
Password  (hidden)
Type the same password again to confirm database creation.
Password  (hidden)
Connected

sql> quit
Connection closed

```

Ceci peut être utilisé:
* Pour vérifier la syntaxe des commandes SQL de création de table
* Pour vérifier l'état de la base de données après exécuton (et cloture) du serveur

Note: il existe une possibilité d'utiliser un serveur sur le port 8082 (et org.h2.tools.service) mais la création d'une nouvelle base est asujetti à la présence d'une option de la ligne de commande
(option qui provoque une brèche de sécurité http://www.h2database.com/html/tutorial.html) 
